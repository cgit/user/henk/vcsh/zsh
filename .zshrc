# zprof only makes sense at the very top
# zmodload zsh/zprof


# settings for the zsh don’t get exported

# where to write history
HISTFILE=~/.zsh_history
HISTSIZE=1000000000
SAVEHIST=1000000000

# print time needed for execution when it takes longer than this value
REPORTTIME=10


## define word characters (for stuff like backward-word, forward-word, backward-kill-word,..)
## these are NOT separators but chars considered part of a word
## these are NOT respected by e.g. vi-backward-kill-word, it seems
# WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>' # the default
## see below how to solve this with select-word-style module


# where zsh looks for functions

# add my own functions
fpath=(~/.zsh_functions $fpath)


# hashed directories are usually independent and standalone
# so they can stay at the top
hash -d diary=~/Data/personal/henk/workinprogress/Dokumente/diary
hash -d lifedocs=~/Data/personal/henk/workinprogress/Dokumente/Leben
hash -d bewerbung=~/Data/personal/henk/workinprogress/Dokumente/Arbeit/bewerbung
hash -d rezepte=~/Data/personal/henk/workinprogress/Dokumente/rezepte/cuisine
hash -d templates=~/Data/personal/henk/workinprogress/templates
hash -d gitignore=~/Data/public/software/gitignore
hash -d doc=/usr/share/doc
hash -d TODO=~/Data/personal/henk/TODO/
hash -d fortunes=~/Data/personal/henk/workinprogress/Dokumente/fortunes/


# aliases are usually independent and standalone
source ~/.zsh_aliases


# load plugin manager after all 'static' stuff is done
source "${HOME}/.zgenom/zgenom.zsh"
# Check for plugin and zgenom updates every 7 days
# This does not increase the startup time.
zgenom autoupdate


# mostly static stuff, env vars
zgenom load 'https://github.com/joel-porquet/zsh-dircolors-solarized'
zgenom load "https://github.com/greymd/tmux-xpanes"


# options are usually independent and standalone
# so they can stay at the top
setopt AUTO_CD
setopt CHASE_LINKS

# switch +/- when referring to the dirstack
setopt PUSHD_MINUS

# Make cd push the old directory onto the directory stack
setopt AUTO_PUSHD

setopt AUTO_NAME_DIRS

# this should not be needed ever
# unclear why it is even implemented
# all relevant functionality should be possible without this anyway
# see #zsh@libera on 2024-04-14
# setopt BRACE_CCL
setopt EXTENDED_GLOB
setopt MARK_DIRS
setopt NUMERIC_GLOB_SORT
setopt RC_EXPAND_PARAM
#setopt WARN_CREATE_GLOBAL
setopt GLOB_DOTS

# Ignore a duplicate of the previous command
setopt HIST_IGNORE_DUPS
# Do not save commands starting with a space to history
setopt HIST_IGNORE_SPACE
# Remove superfluous blanks
setopt HIST_REDUCE_BLANKS
# expand history expansion for verification
setopt HIST_VERIFY
# save timestamp and runtime as well
setopt EXTENDED_HISTORY
# write line to history file when finished, not when started, to have correct runtime
setopt INC_APPEND_HISTORY_TIME

# Recognize comments on the command line
setopt INTERACTIVE_COMMENTS

# Error on a redirections which would overwrite an existing file
setopt NO_CLOBBER
setopt CORRECT
setopt CORRECT_ALL
#unsetopt NOMATCH
setopt PRINT_EXIT_VALUE

setopt AUTO_CONTINUE
setopt NO_BG_NICE
# display PID when suspending processes as well
setopt LONG_LIST_JOBS
# Report the status of background jobs immediately, rather than waiting until just before printing a prompt.
setopt NOTIFY
# Whenever  a  command completion or spelling correction is attempted, make sure the entire command path is hashed first.  This makes the first completion slower but avoids false reports of spelling errors.
setopt HASH_LIST_ALL
# If unset, the cursor is set to the end of the word if completion is started. Otherwise it stays there and completion is done from both ends.
setopt COMPLETE_IN_WORD
# use zsh field splitting
setopt NO_SH_WORD_SPLIT
# unset parameters should cause an error
# CONFLICTS WITH ZINIT
# CONFLICTS WITH ZGENOM
# setopt NO_UNSET


# load before completion system initialization
zgenom load 'https://github.com/zsh-users/zsh-completions'

# initialize completion system before loading plugins and external configs
source ~/.zsh_comprc


# TODO reconsider this
# source /usr/share/autojump/autojump.sh


# autoload all functions defined by me
# since only filenames are required we
# * follow symlinks
# * work only on plainfiles
# * take only the filename part without dir
autoload ~/.zsh_functions/[^_]*(-.:t)


autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git

# similar to xargs
autoload -U zargs

# similar to rename or mmv
autoload -U zmv
autoload -U zln
autoload -U zcp


# zle customization

# spawn editor on current command line
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd '^X^E' edit-command-line
bindkey -M viins '^X^E' edit-command-line

# show globbing results live
autoload insert-files
zle -N insert-files
bindkey '^Xf' insert-files


#this mainly sets vars, binds keys, does zle stuff
# zgenom load 'https://github.com/laurenkt/zsh-vimto'

# functions and an autoload, should be fine here
zgenom load 'https://github.com/MichaelAquilina/zsh-you-should-use'


# source ~/.zsh_taskwarrior
# source ~/.zsh_functions
# source ~/.zsh_functions_media
# source ~/.zsh_functions_taskwarrior


for f in ~/.zsh_local*(N)
do
	source $f
done


#zle -N zle-line-init
#zle -N zle-keymap-select


# keybindings might have been changed by plugins, etc.
# so they go more to the bottom

# the vi bindings for these do not respect WORDCHARS and are not replaced by the select-word-style functions
bindkey -M vicmd 'w' forward-word
bindkey -M vicmd 'b' backward-word
bindkey -M viins '^W' backward-kill-word

# default vi history search is strange
bindkey -M viins '^R' history-incremental-search-backward


# better word matching
autoload -U select-word-style
select-word-style normal
# these characters do _not_ separate words but are part of words
zstyle ':zle:*' word-chars '*?[]~;!#$%^(){}<>'


LP_ENABLE_TIME=1
# should probably be closer to the bottom than the top?
zgenom load 'https://github.com/nojhan/liquidprompt'


# needs to be loaded close to the end because it hooks into other stuff
# see https://github.com/zsh-users/zsh-syntax-highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

eval "$(zoxide init zsh)"

FZF_TMUX=1
FZF_CTRL_T_COMMAND='command fdfind --hidden'
FZF_ALT_C_COMMAND='command fdfind --hidden --type d'
FZF_DEFAULT_COMMAND='command fdfind --hidden'
source /usr/share/doc/fzf/examples/key-bindings.zsh
source /usr/share/doc/fzf/examples/completion.zsh

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fdfind --hidden --follow --exclude ".git" . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fdfind --type d --hidden --follow --exclude ".git" . "$1"
}

# commands to run after initialization is complete
#task awaiting
cat ~/.motd
# zprof
