source ~/.env


# remove duplicates because tmux starts login shells
typeset -U path
export PATH


# zsh you-should-use
YSU_MODE=ALL

